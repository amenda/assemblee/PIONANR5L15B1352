# Chapitre Ier

# Mesures de police administrative

## Article 1er

Après l’article L. 211‑3 du code de la sécurité intérieure, il est inséré un article L. 211‑3‑1 ainsi rédigé :

« Art. L. 211‑3‑1. – Si les circonstances font craindre des troubles d’une particulière gravité à l’ordre public et à compter du jour de déclaration d’une manifestation sur la voie publique, ou si la manifestation n’a pas été déclarée, dès qu’il en a connaissance, le représentant de l’État dans le département ou, à Paris, le préfet de police peut autoriser, par arrêté motivé, pendant les six heures qui précèdent la manifestation et jusqu’à dispersion, à l’entrée et au sein d’un périmètre délimité, les agents mentionnés aux 2° à 4° de l’article 16 du code de procédure pénale et, sous la responsabilité de ces agents, ceux mentionnés à l’article 20 et aux 1°, 1° bis et 1° ter de l’article 21 du même code à procéder, avec le consentement des personnes faisant l’objet de ces vérifications, à des palpations de sécurité ainsi qu’à l’inspection visuelle et à la fouille des bagages. La palpation de sécurité est effectuée par une personne de même sexe que la personne qui en fait l’objet.

« L’arrêté est transmis sans délai au procureur de la République et communiqué au maire de la commune concernée.

« L’arrêté définit le périmètre concerné, qui se limite aux lieux de la manifestation, à leurs abords immédiats et à leurs accès, ainsi que sa durée. L’étendue et la durée du périmètre sont adaptées et proportionnées aux nécessités que font apparaître les circonstances.

« L’arrêté prévoit les règles d’accès et de circulation des personnes dans le périmètre, en les adaptant aux impératifs de leur vie privée, professionnelle et familiale.

« Les personnes qui refusent de se soumettre, pour accéder ou circuler à l’intérieur de ce périmètre, aux palpations de sécurité, à l’inspection visuelle ou à la fouille de leurs bagages, ou qui détiennent, sans motif légitime, des objets pouvant constituer une arme au sens de l’article 132‑75 du code pénal, en infraction à un arrêté pris en application de l’article L. 211‑3 du présent code, s’en voient interdire l’accès ou sont reconduites d’office à l’extérieur du périmètre par les agents mentionnés au premier alinéa du présent article. »

## Article 2

La section 1 du chapitre Ier du titre Ier du livre II du code de la sécurité intérieure est complétée par un article L. 211‑4‑1 ainsi rédigé :

« Art. L. 211‑4‑1. – Le représentant de l’État dans le département ou, à Paris, le préfet de police peut, par arrêté motivé, interdire de prendre part à une manifestation déclarée ou dont il a connaissance à toute personne à l’égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace d’une particulière gravité pour l’ordre public et qui soit s’est rendue coupable, à l’occasion d’une ou plusieurs manifestations sur la voie publique, des infractions mentionnées aux articles 222‑7 à 222‑13, 222‑14‑2, 322‑1 à 322‑3, 322‑6 à 322‑10 et 431‑9 à 431‑10 du code pénal, soit appartient à un groupe ou entre en relation de manière régulière avec des individus incitant, facilitant ou participant à la commission de ces mêmes faits.

« Le représentant de l’État dans le département ou, à Paris, le préfet de police peut imposer, par l’arrêté mentionné au premier alinéa du présent article, à la personne concernée par cette mesure de répondre, au moment de la manifestation, aux convocations de toute autorité ou de toute personne qualifiée qu’il désigne. Cette obligation doit être proportionnée au comportement de la personne.

« L’arrêté précise la manifestation concernée ainsi que l’étendue géographique de l’interdiction, qui doit être proportionnée aux circonstances et qui ne peut excéder les lieux de la manifestation et leurs abords immédiats ni inclure le domicile ou le lieu de travail de la personne intéressée. La durée de l’interdiction ne peut excéder celle de la manifestation concernée.

« L’arrêté est notifié à la personne concernée au plus tard quarante‑huit heures avant son entrée en vigueur.

« Le fait pour une personne de participer à une manifestation en méconnaissance de l’interdiction prévue au premier alinéa est puni de six mois d’emprisonnement et de 7 500 € d’amende.

« Le fait pour une personne de méconnaître l’obligation mentionnée au deuxième alinéa est puni de trois mois d’emprisonnement et de 3 750 € d’amende. »

## Article 3

La section 1 du chapitre Ier du titre Ier du livre II du code de la sécurité intérieure est complétée par un article L. 211‑4‑2 ainsi rédigé :

« Art. L. 211‑4‑2. – Le ministre de l’intérieur et le ministre de la justice sont autorisés à mettre en œuvre un traitement automatisé de données à caractère personnel, afin d’assurer le suivi, au niveau national, des personnes faisant l’objet d’une interdiction de participer à une manifestation sur la voie publique en application de l’article L. 211‑4‑1 du présent code ou de l’article 131‑32‑1 du code pénal.

« Sont enregistrées dans le traitement, dans la stricte mesure où elles sont nécessaires à la poursuite de la finalité mentionnée au premier alinéa du présent article, les données concernant les personnes faisant l’objet d’un arrêté d’interdiction de manifester sur la voie publique en application de l’article L. 211‑4‑1 du présent code ou condamnées à la peine d’interdiction de participer à des manifestations sur la voie publique dans les conditions prévues à l’article 131‑32‑1 du code pénal.

« Les modalités d’application du présent article, y compris la nature des informations enregistrées, la durée de leur conservation ainsi que les autorités et les personnes qui y ont accès, sont déterminées par décret en Conseil d’État pris après avis publié et motivé de la Commission nationale de l’informatique et des libertés. »

# Chapitre II

# Dispositions pénales

## Article 4

Après l’article 431‑9 du code pénal, il est inséré un article 431‑9‑1 ainsi rédigé :

« Art. 431‑9‑1. – Le fait pour une personne, au sein ou aux abords immédiats d’une manifestation sur la voie publique, de dissimuler volontairement, totalement ou partiellement, son visage afin de ne pas être identifiée dans des circonstances faisant craindre des atteintes à l’ordre public est puni d’un an d’emprisonnement et de 15 000 € d’amende.

« Le présent article n’est pas applicable aux manifestations conformes aux usages locaux ou lorsque la dissimulation du visage est justifiée par un motif légitime. »

## Article 5

I. – L’article 431‑10 du code pénal est ainsi rédigé :

« Art. 431‑10. – Est puni de trois ans d’emprisonnement et de 45 000 € d’amende :

« 1° Le fait d’introduire ou de porter une arme ou, sans motif légitime, tout objet susceptible de constituer une arme au sens de l’article 132‑75, y compris des fusées et artifices, dans une réunion publique, dans une manifestation sur la voie publique ou à ses abords immédiats ;

« 2° (Supprimé)

« 3° Le fait de jeter un projectile présentant un danger pour la sécurité des personnes dans une manifestation sur la voie publique.

« La tentative de ces délits est punie des mêmes peines. »

II. – À l’article 431‑12 du code pénal, les mots : « de l’infraction définie » sont remplacés par les mots : « des infractions définies ».

## Article 6

I. – Le code pénal est ainsi modifié :

1° (nouveau) Après l’article 131‑32, il est inséré un article 131‑32‑1 ainsi rédigé :

« Art. 131‑32‑1. – La peine d’interdiction de participer à des manifestations sur la voie publique, qui ne peut excéder une durée de trois ans, emporte défense de manifester sur la voie publique dans certains lieux déterminés par la juridiction. La liste de ces lieux peut être modifiée par le juge de l’application des peines, dans les conditions fixées par le code de procédure pénale.

« La peine d’interdiction de participer à des manifestations sur la voie publique emporte également, pour le condamné, l’obligation de répondre, le temps des manifestations, aux convocations de toute autorité publique désignée par la juridiction de jugement. La décision de condamnation fixe le type de manifestations concernées. Cette obligation doit être proportionnée au regard du comportement de la personne.

« Si la peine d’interdiction de participer à des manifestations sur la voie publique accompagne une peine privative de liberté sans sursis, elle s’applique à compter du jour où la privation de liberté a pris fin. » ;

2° (nouveau) Après le premier alinéa de l’article 222‑47, il est inséré un alinéa ainsi rédigé :

« Dans les cas prévus aux articles 222‑7 à 222‑13 et 222‑14‑2, lorsque les faits sont commis lors du déroulement de manifestations sur la voie publique, peut être prononcée la peine complémentaire d’interdiction de participer à des manifestations sur la voie publique, dans les conditions prévues à l’article 131‑32‑1. » ;

3° (nouveau) Le I de l’article 322‑15 est complété par un 7° ainsi rédigé :

« 7° L’interdiction de participer à des manifestations sur la voie publique, dans les conditions prévues à l’article 131‑32‑1, lorsque les faits punis par les articles 322‑1 à 322‑3 et 322‑6 à 322‑10 sont commis lors du déroulement de manifestations sur la voie publique. » ;

4° Le I de l’article 431‑11 est ainsi modifié :

a) Au premier alinéa, la référence : « par l’article 431‑10 » est remplacée par les mots : « à la présente section » ;

b) Le 2° est ainsi rétabli :

« 2° L’interdiction de participer à des manifestations sur la voie publique, dans les conditions prévues à l’article 131‑32‑1 ; »

5° (nouveau) Après l’article 434‑38, il est inséré un article 434‑38‑1 ainsi rédigé :

« Art. 434‑38‑1. – Le fait, pour une personne condamnée à une peine d’interdiction de participer à des manifestations sur la voie publique, de participer à une manifestation en méconnaissance de cette interdiction est puni d’un an d’emprisonnement et de 15 000 € d’amende.

« Le fait, pour une personne condamnée à une peine d’interdiction de participer à des manifestations sur la voie publique, de ne pas répondre, le temps des manifestations, aux convocations de toute autorité publique désignée par la juridiction de jugement, en méconnaissance de la décision de condamnation, est puni de six mois d’emprisonnement et de 7 500 € d’amende. »

II. – L’article L. 211‑13 du code de la sécurité intérieure est abrogé.

# Chapitre III

# Responsabilité civile

## Article 7

Après le premier alinéa de l’article L. 211‑10 du code de la sécurité intérieure, il est inséré un alinéa ainsi rédigé :

« L’État peut exercer une action récursoire contre les personnes ayant participé à tout attroupement ou rassemblement armé ou non armé, lorsque leur responsabilité pénale a été reconnue par une décision de condamnation devenue définitive. »

# Chapitre IV

# Application outre‑mer

(Division et intitulé nouveaux)

## Article 8 (nouveau)

I. – L’article 711‑1 du code pénal est ainsi rédigé :

« Art. 711‑1. – Sous réserve des adaptations prévues au présent titre, les livres Ier à V du présent code sont applicables, dans leur rédaction résultant de la loi n°       du       visant à prévenir les violences lors des manifestations et à sanctionner leurs auteurs, en Nouvelle‑Calédonie, en Polynésie française et dans les îles Wallis et Futuna. »

II. – Le premier alinéa de l’article 804 du code de procédure pénale est ainsi rédigé :

« Le présent code est applicable, dans sa rédaction résultant de la loi n°       du       visant à prévenir les violences lors des manifestations et à sanctionner leurs auteurs, en Nouvelle‑Calédonie, en Polynésie française et dans les îles Wallis et Futuna, sous réserve des adaptations prévues au présent titre et aux seules exceptions : ».

III. – Au premier alinéa des articles L. 285‑1, L. 286‑1 et L. 287‑1 du code de la sécurité intérieure, la référence : « n° 2017‑1510 du 30 octobre 2017 renforçant la sécurité intérieure et la lutte contre le terrorisme » est remplacée par la référence : « n°       du       visant à prévenir les violences lors des manifestations et à sanctionner leurs auteurs ».

IV (nouveau). – Aux articles L. 282‑1 et L. 284‑1 du code de la sécurité intérieure, la référence : « L. 211‑13, » est supprimée.